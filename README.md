## Courtney Meddaugh's README

Hey there :wave: Welcome to my GitLab profile!

At GitLab, I work as a [Principal Product Manager](https://handbook.gitlab.com/job-families/product/product-manager/#principal-product-manager) in our [Fulfillment section](https://about.gitlab.com/handbook/engineering/development/fulfillment/), which strives to provide our customers with a world-class buyer experience through the products we build. I've been at GitLab since January 2021 and am located in Austin, TX! 

### Provision Team
As a Product Manager, I support the [Provision Group](https://about.gitlab.com/direction/fulfillment/provision/), which aims to provide a seamless customer experience in accessing GitLab subscriptions, add-ons and trials, while providing key license delivery and usage data to internal teams for data-driven insights. For details on our current and future planned work, check out our [epic roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=MONTHS&timeframe_range_type=CURRENT_YEAR&label_name[]=Fulfillment+Roadmap&label_name[]=group::provision&progress=WEIGHT&show_progress=true&show_milestones=false&milestones_type=GROUP&show_labels=false).

### How I Work
I work primarily in GitLab issues and epics, and use Slack for quick collaboration and broadcasting. I focus first on `to-dos`, and then refer to non-mentions via email. I also use my own scoped labels to track issues I'm working on in an Issue Board.

I'm always open to meeting new people, so feel free to schedule a coffee chat (internal) or shoot me an email (external), or [add me on LinkedIn](https://www.linkedin.com/in/courtney-meddaugh/) if you're interested in learning more about my role or team!
